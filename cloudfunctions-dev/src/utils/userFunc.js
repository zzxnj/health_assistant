
const db = uniCloud.database();
const collection1 = db.collection('ha_group_person');

async function checkGroupMember(group_id,member_id) {
	let user = await collection1
		.where({
			_id: member_id,
			group_id
		})
		.get();

	if (user.data && user.affectedDocs >= 1) {
		return true
	}
	return false;
}

async function updateGroupMember(member_id,member_info) {
	await collection1
		.doc(member_id)
		.update(member_info);
}

export default {
	checkGroupMember,
	updateGroupMember
}