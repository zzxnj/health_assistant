
const db = uniCloud.database();
const collection1 = db.collection('ha_report_list');
const collection2 = db.collection('ha_report_record');
const collection3 = db.collection('ha_group_person');

async function checkReportRecord(report_id,gp_id) {
	let res1 = await collection1
		.doc(report_id)
		.get();

	if (res1.data && res1.affectedDocs === 1) {
	} else {
		return false;
	}

	let res2 = await collection2
		.where({
			report_id,
			gp_id
		})
		.orderBy("create_time", "desc")
		.limit(1)
		.get();

	if (res2.data && res2.affectedDocs >= 1) {
		if (res2.data[0].allow_modify === 1) {
			return true;
		}

		// 检查间隔时间
		if (res1.data[0].interval_hour > 0) {
			if (Date.now() >= (res2.data[0].create_time + 3600000 * res1.data[0].interval_hour)) { // 一小时=3600000
				return true;
			}
		}

		return false;
	}

	return true;
}

async function getOneRecord(record_id) {
	let res2 = await collection2
		.doc(record_id)
		.get();

	if (res2.data && res2.affectedDocs === 1) {
	} else {
		return false;
	}

	if (res2.data[0].hasOwnProperty('report_id') == false) {
		return false;
	}

	let res1 = await collection1
		.doc(res2.data[0].report_id)
		.get();

	if (res1.data && res1.affectedDocs === 1) {
	} else {
		return false;
	}

	return {
		schema: res1.data[0].content,
		model: res2.data[0].model
	}
}

async function getReportRecord1(report_id,condition = {}) {
	let res2 = await collection2
		.where(
			Object.assign({
				report_id
			},condition)
		)
		.orderBy("create_time", "asc")
		.get();

	if (res2.data && res2.affectedDocs >= 1) {
	} else {
		return false;
	}

	let recordList = [];
	for (var i = 0; i < res2.data.length; i++) {
		let res3 = await collection3
			.doc(
				res2.data[i].gp_id
			)
			.get();

		let person_name = "未知";
		let person_identity = "";
		if (res3.data.length) {
			person_name = res3.data[0].person_name;
			person_identity = res3.data[0].person_identity;
		}

		recordList.push(
			Object.assign({
				person_name,
				person_identity,
				create_time: res2.data[i].create_time
			},
			res2.data[i].model
			)
		);
	}

	return recordList;
}

async function getReportList(group_id) {
	let res = await collection1
		.where({
			group_id
		})
		.get();

	return res.data;
}

async function checkReportName(group_id,name) {
	let res = await collection1
		.where({
			group_id,
			name
		})
		.get();

	if (res.data && res.affectedDocs >= 1) {
		return false;
	} else {
		return true;
	}
}

async function getReportUndo(report_id,condition = {}) {
	let res1 = await collection1
		.doc(report_id)
		.get();

	if (res1.data && res1.affectedDocs === 1) {
	} else {
		return false;
	}

	let res3 = await collection3
		.where({
			group_id: res1.data[0].group_id
		})
		.get();

	let personUndo = [];
	for (var i = 0; i < res3.data.length; i++) {
		let res2 = await collection2
			.where(
				Object.assign({
					report_id,
					gp_id: res3.data[i]._id
				},condition)
			)
			.get();

		if (res2.data && res2.affectedDocs >= 1) {
		} else {
			personUndo.push({person_name: res3.data[i].person_name});
		}
	}

	return personUndo;
}

async function getReportRecord2(report_id,page,length) {
	const result = await collection2
		.where({
			report_id
		})
		.orderBy("create_time","desc")
		.skip(length * page)
		.limit(length)
		.get();

	const recordList = [];
	for (var i = 0; i < result.data.length; i++) {
		const {
			_id,
			gp_id,
			create_time,
			model
		} = result.data[i];

		let tmp = await collection3
			.doc(gp_id)
			.get();

		let person_name = "未知";
		if (tmp.data.length) {
			person_name = tmp.data[0].person_name;
		}

		recordList.push({
			_id,
			gp_id,
			person_name,
			create_time,
			model
		});
	}

	return recordList;
}

export default {
	checkReportRecord,
	getOneRecord,
	getReportRecord1,
	getReportList,
	checkReportName,
	getReportUndo,
	getReportRecord2
}