import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import accessFunc from '../../utils/accessFunc.js';

const db = uniCloud.database();
const collection = db.collection('ha_access_list');

async function Perform(event) {
	if (event.type === 0) { // list
		let res = await collection
			.where({
				group_id: event.group_id
			})
			.get();

		return res.data;
	} else if (event.type === 1) { // detail
		let res = await collection
			.doc(
				event.access_id
			)
			.get();

		if (res.data && res.affectedDocs === 1) {
		} else {
			return {
				status: -1,
				msg: "抱歉，出入点不存在！"
			}
		}

		// 出入点进出记录
		let record = await accessFunc.getAccessRecord(event.access_id,0,10);

		return {
			status: 0,
			data: {
				self: res.data[0],
				record: record
			},
			msg: "获取成功"
		}
	} else if (event.type === 2) { // one
		let res = await collection
			.doc(event.access_id)
			// .where({
			// 	_id: event.access_id,
			// 	is_active: true
			// })
			.get();

		if (res.data && res.affectedDocs === 1) {
		} else {
			return {
				status: -1,
				msg: "抱歉，出入点不存在！"
			}
		}

		return {
			status: 0,
			data: res.data[0],
			msg: "获取成功"
		}
	} else if (event.type === 10) { // check
		const {
			access_id,
			check_code
		} = event;
		let pass = await accessFunc.checkAccessItem(access_id,{check_code});

		return {
			status: 0,
			pass,
			msg: "验证结束"
		}
	}
}

export {
	Perform as main
}
