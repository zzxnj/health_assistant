import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import validateToken from '../../utils/validateToken.js';
const db = uniCloud.database();

async function Perform(event) {
	if (!event.token) {
		return {
			status: -4,
			errCode: 'TOKEN_INVALID',
			msg: '出错了！'
		}
	}

	try {
		let result = await validateToken(event.token);
		if (result.status != 0) {
			return result;
		}
	} catch (e) {
		return {
			status: -2,
			errCode: 'TOKEN_INVALID',
			// msg: 'token无效'
			msg: '请重新登录'
		}
	}

	if (!event.memberId) {
		return {
			status: -10,
			msg: "当前成员ID不存在！"
		}
	}

	const member = db.collection("ha_group_person");
	let delData = await member.doc(event.memberId).remove();

	if (delData.affectedDocs && delData.deleted) {
		return {
			status: 0,
			msg: "删除成员成功！"
		}
	} else {
		return {
			status: -11,
			msg: "网络异常，请稍后重试！"
		}
	}
}

export {
	Perform as main
}
