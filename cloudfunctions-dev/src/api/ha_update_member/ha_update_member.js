import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import userFunc from '../../utils/userFunc.js';

async function Perform(event) {
	if (!event.member_id) {
		return {
			status: -1,
			msg: '出错了！'
		}
	}

	if (!event.group_id) {
		return {
			status: -2,
			msg: "当前团队id错误，请返回！"
		};
	}

	if (!event.person_name) {
		return {
			status: -2,
			msg: "成员名字有误！"
		};
	}

	if (!event.person_identity || !(/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(event.person_identity))) {
		return {
			status: -2,
			msg: "成员身份证号有误，请重新输入！"
		};
	}

	if (!event.person_sex) {
		return {
			status: -2,
			msg: "成员性别不能为空！"
		};
	}

	// if (!event.person_num) {
	// 	return {
	// 		status: -2,
	// 		msg: "成员编号有误！"
	// 	};
	// }

	// 检查成员和团体的关系是否正确
	let isMember = await userFunc.checkGroupMember(event.group_id,event.member_id);

	if (isMember != true) {
		return {
			status: -1,
			msg: '出错了！'
		}
	}
	
	let member_id = event.member_id;
	delete event.member_id
	delete event.group_id
	await userFunc.updateGroupMember(member_id,event);

	return {
		status: 0,
		msg: "更新成功！"
	}
}

export {
	Perform as main
}
