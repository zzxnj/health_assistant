import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import utils from '../../utils/utils.js';
import accessFunc from '../../utils/accessFunc.js';

async function Perform(event) {
	if (event.type === 0) { // 是否激活
		let {
			access_id,
			is_active
		} = event;

		await accessFunc.updateAccessItem(access_id,{
			is_active
		});
	} else if (event.type === 1) { // 更新管理人员验证码
		let {
			access_id
		} = event;

		let check_code = utils.random(10000000,99999999);
		await accessFunc.updateAccessItem(access_id,{
			check_code
		});

		return {
			status: 0,
			check_code,
			msg: "更新成功！"
		}
	}

	return {
		status: 0,
		msg: "更新成功！"
	}
}

export {
	Perform as main
}
