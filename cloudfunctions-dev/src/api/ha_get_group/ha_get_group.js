import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import reportFunc from '../../utils/reportFunc.js';
import groupFunc from '../../utils/groupFunc.js';

const db = uniCloud.database();
const collection = db.collection('ha_group_list');

async function Perform(event) {
	if (event.type === 0) { // list
		let group_list = await collection
			.where({
			account_id: event.account_id
			})
			.get();
		if (group_list.data) {
			return {
				code: 0,
				data: group_list.data
			};
		} else {
			return {
				code: -1,
				msg: "网络错误！"
			};
		}
	} else if (event.type === 1) { // detail
		let res = await collection
			.doc(
				event.group_id
			)
			.get();

		if (res.data && res.affectedDocs === 1) {
		} else {
			return {
				status: -1,
				msg: "团体不存在！"
			}
		}

		let reportList = await reportFunc.getReportList(event.group_id);
		let childGroup = await groupFunc.getFirstChildGroup(event.group_id);

		return {
			status: 0,
			self: res.data[0],
			reportList,
			childGroup,
			msg: "获取成功"
		}
	} else if (event.type === 2) { // one
		let res = await collection
			.doc(
				event.group_id
			)
			.get();

		if (res.data && res.affectedDocs === 1) {
			if (res.data.length === 1) {
				return {
					status: 0,
					data: res.data[0],
					msg: "获取成功"
				}
			}
		}

		return {
			status: -1,
			msg: "团体不存在"
		}
	}
}

export {
	Perform as main
}
