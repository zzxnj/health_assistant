import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();
const collection = db.collection('ha_report_record');

async function Perform(event) {
	if (event.type === 0) {
		await collection
			.doc(event.record_id)
			.update({
				allow_modify: 1
			});
	} else if (event.type === 1) {
		// nothing
	}

	return {
		status: 0,
		msg: "更新成功！"
	}
}

export {
	Perform as main
}
