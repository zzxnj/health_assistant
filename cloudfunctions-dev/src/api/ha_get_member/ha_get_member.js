import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();
const collection = db.collection('ha_group_person');

async function Perform(event) {
	if (event.type === 0) { // list
		const ha_group_list = db.collection("ha_group_list");
		const ha_group_person = db.collection("ha_group_person");

		//event为客户端上传的参数

		//   ha_group_person
		// {
		//     _id: "", // string，自生成
		//     group_id: "", // string 团体ID
		//     person_name: "", // string 成员名字
		//     person_identity: "", // string 成员身份证号
		//     person_sex: 0, // int, 性别1男2女3保密
		//     person_num: 0, // int 成员编号（学号、工号等）
		//     person_remark: "", // string 成员备注（如部门中的某分组不再建立团体，就写入备注）
		//     order: 1, // int，排序
		// }

		// 先查询有没有团队
		let group_list = await ha_group_list
			.where({
				_id: event.group_id
			})
			.get();

		// 能查到团队记录
		if (group_list.data.length) {
			// 接着查询团队有没有成员
			let member = await ha_group_person
				.where({
					group_id: event.group_id
				})
				.get();

			// 有成员就展示
			if (member.data.length) {
				return {
					code: 0,
					data: member.data
				};
			} else {
				// 没有成员就显示添加按钮，添加成员
				return {
					code: -1,
					msg: "没有查到成员记录！"
				};
			}
		} else {
			// 查不到团队记录
			return {
				code: -2,
				msg: "团队id错误，返回重新选择团队！"
			};
		}
	} else if (event.type === 1) { // one member detail
		let user = await collection
			.doc(
				event.member_id
			)
			.get();

		if (user.data && user.affectedDocs === 1) {
		} else {
			return {
				status: -1,
				msg: "抱歉，成员不存在！"
			}
		}

		return {
			status: 0,
			data: user.data[0],
			msg: "获取成功"
		}
	}
}

export {
	Perform as main
}
