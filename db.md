**数据表使用 ha_ 前缀**

## 团体表

团体列表：班级、公司、部门
页面使用group_id传递

```
ha_group_list
{
    _id: "", // string，自生成
    parent_id: "", // 所属ID，上级团体，本表的_id字段值
    name: "", // 团体名称，如"四（3）班"、"软件2组"、"财务部"、"xx公司"等
    label: "", // 标签，提供几个常用的选择，如学校、班级、公司、部门等
    account_id: "", // string 管理员账号ID（用户账号表ha_user_account）
    is_reg: true, // int 是否开放成员注册 true开放
    is_split: false, // int 是否分开 true分开：断开本团体的上级和下级的关系，公司、学校等使用
    order: 1, // int，排序
}
```

团体成员名单：团体管理员导入的名单

```
ha_group_person
{
    _id: "", // string，自生成
    group_id: "", // string 团体ID
    person_name: "", // string 成员名字
    person_identity: "", // string 成员身份证号
    person_sex: 0, // int, 性别1男2女3保密
    person_num: 0, // int 成员编号（学号、工号等）
    person_remark: "", // string 成员备注（如部门中的某分组不再建立团体，就写入备注）
    // 其他需要备注的信息：如按楼组成的团体，可写入门牌号码
    order: 1, // int，排序
}
```

团体通知：暂时不用

```
ha_group_notice
{
    _id: "", // string，自生成
    group_id: "", // string 团体ID
    order: 1, // int，排序
}
```

## 用户表

用户账号表：本地存储为account_id

```
ha_user_account
{
    _id: "", // string，自生成
    username: "", // string 用户
    password: "", // string 密码(禁止明文)
    nickname: "", // string 昵称
    mobile: "", // string 手机号，需验证符合规则
    email: "", // string 邮箱
    wx_open_id: "", // string 关联微信openid  用户code 换取 暂不用，后续和支付宝等一起考虑 TODO
}
```

个人信息表：暂时不用，用于个人录入人员，自己的健康档案

```
ha_user_person
{
    _id: "", // string，自生成
    name: "", // string 用户名字
    identity: "", // string 身份证号
    sex: 0, // int, 性别1男2女3保密
    symbol: "" // string 关系、备注等
}
```

用户的报备模板：包含导入自建、收藏、上级团队下发的等

```
ha_user_template
{
    _id: "", // string，自生成
    account_id: "", // string 用户账号“ha_user_account” ID
    template_id: "", // string 模板“ha_template_list” ID
    get_way: 0, // int 来源 0未知，1导入自建或收藏，2上级下发
}
```

## 关系表

用户账号和个人信息：暂时不用
账号ha_user_account和人员ha_user_person的关联

```
ha_relation_user
{
    _id: "", // string，自生成
    account_id: "", // string 用户账号ID
    person_id: "", // string 个人信息ID
}
```

## 资源表

模板资源：目前主要是报备模板，但也可以用作其他用途的模板

```
ha_template_list
{
    _id: "", // string，自生成
    name: "", // string 模板名称
    <!-- label: "", // string 模板标签 TODO 可以不用 -->
    is_open: 0, // int 是否公开1公开，其他不公开（前端不能改，只有后台才可以设为公开，公开的所有用户都可以看到）
    content: "", // string 模板内容，按json存储，使用时动态生成表单（json schema），查找现成的vue组件
}
```

报备列表：

```
ha_report_list
{
    _id: "", // string，自生成
    group_id: "", // string 所属团体ID
    group2_id: "", // string 发起报备的团体ID
    name: "", // string 报备名称
    is_active: 1, // int 是否激活
    interval_hour: 0, // int 间隔小时数0表示只能提交单次，其他间隔时间可以重复提交
    check_member: 1, // int 是否检查是不是团体成员，0不检查
    content: "", // string 报备内容，按json存储，使用时动态生成表单（json schema）
}
```

报备记录：

```
ha_report_record
{
    _id: "", // string，自生成
    report_id: "", // string 所属reportID
    gp_id: "", // string 团体成员ID，ha_group_person._id
    create_time: 0, // int 时间戳 GMT
    allow_modify: 0, // int 是否允许修改1允许
    // model 以下由报备内容生成的所有字段，考虑使用统一的前缀，和ha_report_list中content存的内容关联
}
```

报备汇总：暂时不用

```
ha_report_summary
{
    _id: "", // string，自生成
    group_id: "", // string 所属团体ID
}
```

出入点列表：

```
ha_access_list
{
    _id: "", // string，自生成
    group_id: "", // string 所属团体ID
    name: "", // string 出入点名称
    address: "", // string 出入点地址
    is_active: true, // int 是否激活
    check_code: "", // string 管理员校验码
    content: "", // string 登记内容，按json存储，使用时动态生成表单（json schema）
}
```

出入登记记录：

```
ha_access_record
{
    _id: "", // string，自生成
    access_id: "", // string 所属accessID
    access_state: 1, // int 1登记进入3同意进入7已离开
    create_time: 0, // int 时间戳 GMT
    // model 以下由出入登记内容生成的所有字段，考虑使用统一的前缀，和ha_access_list中content存的内容关联
}
```
